package org.example.decorator;

import org.example.Message;
import org.example.MessageSender;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Декоратор. Шифрует текст сообщения.
 *
 * @author Tatyana_Dolnikova
 */
public class TextEncoder extends Decorator {

    public TextEncoder(MessageSender messageSender) {
        super(messageSender);
    }

    @Override
    public void sendMessage(Message msg) {
        encode(msg);
        super.sendMessage(msg);
    }

    @Override
    public List<Message> receiveAllMessages() {
        List<Message> messages = super.receiveAllMessages();
        if (messages == null) {
            return null;
        }
        List<Message> decodedMessages = new ArrayList<>();
        for (Message message : messages) {
            if (message == null) {
                continue;
            }
            String decodedText = decode(message.getText());
            message.setText(decodedText);
            decodedMessages.add(message);
        }
        return decodedMessages;
    }

    private void encode(Message msg) {
        String text = msg.getText();
        byte[] result = text.getBytes();
        for (int i = 0; i < result.length; i++) {
            result[i] += (byte) 1;
        }
        String encodedText = Base64.getEncoder().encodeToString(result);
        msg.setText(encodedText);
    }

    private String decode(String data) {
        byte[] result = Base64.getDecoder().decode(data);
        for (int i = 0; i < result.length; i++) {
            result[i] -= (byte) 1;
        }
        return new String(result);
    }

}
