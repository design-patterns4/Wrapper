package org.example.decorator;

import org.example.Message;
import org.example.Sender;

import java.util.List;

/**
 * Базовый декоратор.
 *
 * @author Tatyana_Dolnikova
 */
public class Decorator implements Sender {

    Sender wrapper;

    Decorator(Sender sender) {
        this.wrapper = sender;
    }

    @Override
    public void sendMessage(Message msg) {
        wrapper.sendMessage(msg);
    }

    @Override
    public List<Message> receiveAllMessages() {
        return wrapper.receiveAllMessages();
    }
}
