package org.example.decorator;

import org.example.Message;
import org.example.MessageSender;
import org.example.Sender;

import java.util.List;

/**
 * Декоратор. Шифрует имя отправителя и получателя.
 *
 * @author Tatyana_Dolnikova
 */
public class NameEncoder extends Decorator {

    public NameEncoder(Sender messageSender) {
        super(messageSender);
    }

    @Override
    public void sendMessage(Message msg) {
        hideNames(msg);
        super.sendMessage(msg);
    }

    @Override
    public List<Message> receiveAllMessages() {
        return super.receiveAllMessages();
    }

    private void hideNames(Message msg) {
        if (msg == null) {
            return;
        }
        String author = msg.getAuthor();
        String receiver = msg.getReceiver();
        msg.setAuthor(getHiddenName(author));
        msg.setReceiver(getHiddenName(receiver));
    }

    private String getHiddenName(String name) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            result.append("*");
        }
        return result.toString();
    }

}
