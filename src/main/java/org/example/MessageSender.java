package org.example;

import java.util.ArrayList;
import java.util.List;

/**
 * MessageSender.
 *
 * @author Tatyana_Dolnikova
 */
public class MessageSender implements Sender {

    private List<Message> messages;

    MessageSender() {
        this.messages = new ArrayList<>();
    }

    @Override
    public void sendMessage(Message msg) {
        messages.add(msg);
        System.out.println(msg.getAuthor() + " sent a message to " + msg.getReceiver() + " : " + msg.getText());
    }

    @Override
    public List<Message> receiveAllMessages() {
        return messages;
    }
}
