package org.example;

import java.util.List;

/**
 * Client.
 *
 * @author Tatyana_Dolnikova
 */
public interface Sender {

    void sendMessage(Message msg);

    List<Message> receiveAllMessages();

}
