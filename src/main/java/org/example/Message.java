package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Message.
 *
 * @author Tatyana_Dolnikova
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class Message {

    private String author;
    private String receiver;
    private String text;

}
