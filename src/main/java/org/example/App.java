package org.example;

import org.example.decorator.Decorator;
import org.example.decorator.NameEncoder;
import org.example.decorator.TextEncoder;

import java.util.List;

public class App {

    public static void main(String[] args) {
        MessageSender messageSender = new MessageSender();
        Decorator decorator = new NameEncoder(new TextEncoder(new MessageSender()));

        Message msg1 = new Message("Таня", "Ваня", "Привет!");
        Message msg2 = new Message("Алексей", "Константин", "Привет!");
        Message msg3 = new Message("Яна", "Кристина", "До свидания!");

        System.out.println("-----------------");
        System.out.println("Without Decorator");
        System.out.println("-----------------");
        print(messageSender, msg1, msg2, msg3);

        System.out.println("--------------");
        System.out.println("With Decorator");
        System.out.println("--------------");
        print(decorator, msg1, msg2, msg3);
    }

    private static void print(Sender sender, Message... msg) {
        for (Message message : msg) {
            sender.sendMessage(message);
        }
        List<Message> decodedMessages = sender.receiveAllMessages();
        System.out.println("All messages:");
        for (Message decodedMessage : decodedMessages) {
            System.out.println(decodedMessage);
        }
        System.out.println();
    }

}

